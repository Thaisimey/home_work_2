<div class="table-responsive">
    <table class="table fixed-table">
        <thead>
            <th style="width: 40px;" scope="col">#No</th>
            <th scope="col">Name</th>
            <th scope="col">Subject</th>
            <th scope="col">Email</th>
            <th scope="col">Status</th>
            <th style="width: 90px;" class="text-center" scope="col">Date</th>
            <th scope="col" class="text-center">Action</th>
        </thead>
        <tbody style="height: 370px;">
            @foreach($contacts as $index => $contact)
        <tr class="contact-row" data-id="{{ $index + 1 }}" style="cursor : pointer;">
                <th style="width: 50px" scope="row">#{{ $index + 1 }}</th>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->subject_name }}</td>
                <td style="width: 180px;" >{{ $contact->email }}</td>
                <td>{{ $contact->status }}</td>
                <td style="width: 100px" class="text-center">{{ $contact->date }}</td>
                <td class="text-center">
                    <a style="color: #707070;" href="">Edit</a> | <a style="color: #707070;" href="">Delete</a>
                </td>
    
            </tr>
            @endforeach
            
    
        </tbody>
        
    </table>
    </div>
    
    @push('script')
        <script>
            $(function () {
                $(document).on('click', '.contact-row', function (event) {
                    let id = $(this).data('id');
                    window.location = `/admin/contacts/${id}`;
                })
            })
        </script>
    @endpush
    