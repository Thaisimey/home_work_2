<div class="modal fade" id="{{ $id }}" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-uppercase">{{ $title }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="modal-body">
                <div class="mb-4">
                    {{ $body }}
                </div>
                <div class="d-flex justify-content-between">
                    {{ $actions }}
                </div>
            </form>
        </div>
    </div>
</div>
