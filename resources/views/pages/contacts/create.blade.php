@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact')

@section('body')
<div class="px-1">
    <form class="justify-content-center mt-5">
        <div class="form-group">
            <label style="text-align: left;">Name *</label>
            <input type="text" class="form-control">
        </div>
        <div class="row">
            <div class="form-group col-6">
                <label>Email *</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group col-6">
            <label>Phonenumber *</label>
            <input type="text" class="form-control">
        </div>
            
        </div>
        <div class="form-group">
            <label>Subject</label>
            <input type="text" class="form-control">
        </div>
        <div class="form-group">
            <label>Message</label>
            <input type="text" class="form-control">
        </div>
        <div class="d-flex justify-content-end">
            <a href="{{ route('contacts.confirm') }}" class="btn btn-outline-secondary">Next <i class="fa fa-chevron-right"></i></a>
        </div>
    </form>
</div>

@stop
