@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact Submitted')

@section('body')
        	    <div class="px-1">
					<form class="justify-content-center mt-5">
						<div class="text-center">
						<h4>Your message have been submit to our system</h4>
							<p>Our team will feedback to you soon, Thank you</p>
						</div>
						
						<div class="d-flex justify-content-center mt-5	">
                            <a href="{{ route('contacts.create') }}" class="text-uppercase btn btn-outline-secondary"><i class="fa fa-chevron-left"></i> Go Back Home Page</a>
						</div>
					</form>
				</div>
@stop
